<?php

include_once("../../../vendor/autoload.php");

$education = new \App\BITM\SEIP128330\Education\Education();

$allData = $education->index();

$dataToShowInPDF = "";
$sl = 0;
foreach ($allData as $data):
    $sl++;
    $id = $data['id'];
    $name = $data['name'];
    $level = $data['level'];
    $dataToShowInPDF .= "<tr>";
    $dataToShowInPDF .= "<td> $sl </td>";
    $dataToShowInPDF .= "<td> $id</td>";
    $dataToShowInPDF .= "<td> $name </td>";
    $dataToShowInPDF .= "<td> $level </td>";
    $dataToShowInPDF .= "</tr>";

endforeach;

$html = <<<EDO

<table class="table table-hover">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Name</th>
            <th>level</th>
        </tr>
        </thead>
        <tbody>
        
            $dataToShowInPDF;
        

        </tbody>
    </table>

EDO;

// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('list.pdf', 'D');

?>