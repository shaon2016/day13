<?php
/**
 * Created by PhpStorm.
 * User: Shaon
 * Date: 6/29/2016
 * Time: 3:07 PM
 */

namespace App\BITM\SEIP128330\Education;


include_once('../../../vendor/autoload.php');

use App\BITM\SEIP128330\Message;
use App\BITM\SEIP128330\Utility;

class Education
{
    public $id;
    private $level;
    private $name;
    private $deletedAt;

    private $filterByName;
    private $filterByLevel;
    private $search;
    
    private $con;
    private $alllevelData = array();
    private $allTrashedlevelData = array();

    function __construct()
    {
        $this->con = mysqli_connect('localhost', 'root', "", 'atomicprojectb22');
    }

    // Accessing DB for list to show data
    function index()
    {

        $whereClause= " 1=1 ";
        if(!empty($this->filterByName)){
            $whereClause.="AND name LIKE '%".$this->filterByName."%'";
        }
        if(!empty($this->filterByLevel)){
            $whereClause.="AND level LIKE '%".$this->filterByLevel."%'";
        }
        if(!empty($this->search)){
            $whereClause.="AND level LIKE '%".$this->search."%' OR name LIKE '%".$this->search."%'";
        }
        
        $sql = "SELECT * FROM `education` where deleted_at is null AND ". $whereClause;
        $result = mysqli_query($this->con, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in alllevelData variable and return it to show the data
        // in index.php file
// we must declare alllevelData as array to contain all data
        while ($row = mysqli_fetch_assoc($result))
            $this->alllevelData[] = $row;

        return $this->alllevelData;
    }


    public function prepareVariableValue($data)
    {
        if (array_key_exists('level', $data))
            $this->level = $data['level'];


        if (array_key_exists('name', $data))
            $this->name = $data['name'];

        if (array_key_exists('id', $data))
            $this->id = $data['id'];

        if (array_key_exists('search', $data))
            $this->search = ($data['search']);

        if (array_key_exists('filterByLevel', $data))
            $this->filterByLevel = ($data['filterByLevel']);

        if (array_key_exists('filterByName', $data))
            $this->filterByName = ($data['filterByName']);

        return $this;
    }

    function store()
    {

        $sql = "INSERT INTO `atomicprojectb22`.`education` (`name`, `level`)
 VALUES ('".$this->name."', '".$this->level."')";

        $result = mysqli_query($this->con, $sql);


        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been stored successfully!</strong>
            </div>");

            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has not been stored successfully!</strong>
            </div>");


            Utility::reDirectAPageIntoAnotherPage("index.php");
        }

        //echo $result;
        // echo $this->level;

        //return " I am storing data ";
    }


    function update()
    {
        $query = "UPDATE `education` SET `name` = '" . $this->name . "', `level` = '" . $this->level . "'
        WHERE `education`.`id` = " . $this->id;
        $result = mysqli_query($this->con, $query);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been Updated successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function delete()
    {
        $query = "DELETE FROM `atomicprojectb22`.`education` WHERE `education`.`id` =" . $this->id;

        $result = mysqli_query($this->con, $query);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been deleted successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function view()
    {
        $query = "SELECT * FROM `education` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->con, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }

    function trash()
    {
        $this->deletedAt = time();

        $sql = "UPDATE `education` SET `deleted_at` = '".$this->deletedAt."' WHERE `education`.`id` =". $this->id;
        $result = mysqli_query($this->con, $sql);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been trashed successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function trashed()
    {
        $sql = "SELECT * FROM `education` where deleted_at is not null";
        $result = mysqli_query($this->con, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in alllevelData variable and return it to show the data
        // in index.php file
// we must declare alllevelData as array to contain all data
        while ($row = mysqli_fetch_assoc($result))
            $this->allTrashedlevelData[] = $row;

        return $this->allTrashedlevelData;
    }

    function recover ()
    {
        $sql = "update education set deleted_at = null where id = ". $this->id;
        $result = mysqli_query($this->con, $sql);

        if($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been recovered successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function recoverSelected($ids = array())
    {
        if(is_array($ids) && count($ids) > 0) {
            $IDs = implode(",", $ids);

            $sql = "update education set deleted_at = NULL WHERE id IN (". $IDs .")";
            $result = mysqli_query($this->con, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been recovered successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    function deleteSelected($IDs = array())
    {
        if(is_array($IDs) && count($IDs) > 0) {
            $ids = implode(",", $IDs);

            $sql = 'delete from `education` where id IN ('.$ids.')';

            $result = mysqli_query($this->con, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been Deleted successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `education` WHERE deleted_at IS NULL";
        $result=mysqli_query($this->con,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_alllevel = array();
        $query="SELECT * FROM `education` WHERE deleted_at is null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->con, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_alllevel[] = $row;
        }

        return $_alllevel;

    }
}