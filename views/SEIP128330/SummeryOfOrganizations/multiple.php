<?php

include "../../../vendor/autoload.php";

$summery = new \App\BITM\SEIP128330\SummeryOfOrganizations\SummeryOfOrganizations();


if(array_key_exists('recoverAll', $_POST)) {
    $summery->recoverSelected($_POST['id']);
}


if(array_key_exists('deleteAll', $_POST)) {
    $summery->deleteSelected($_POST['id']);
}