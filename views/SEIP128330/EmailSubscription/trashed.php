<?php
session_start();
include_once("../../../vendor/autoload.php");
use App\BITM\SEIP128330\City\City;
use App\BITM\SEIP128330\Utility;
use App\BITM\SEIP128330\Message;

//Creating object
$email = new \App\BITM\SEIP128330\EmailSubscription\EmailSubscription();
//Getting DB data as object form
$getAllTrashedEmailData = $email->trashed();
// Checking DB data
//Utility::dd($getAllTrashedEmailData);


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Email Subscription Trashed</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Email Trashed List</h2>

    <form action="recover_multiple.php" method="post" id = 'multiple'>

        <a href="index.php" class="btn btn-info" role="button">Go to homepage</a>
        <button type="submit" class="btn btn-info" role="button">Recover Selected</button>
        <button type="button" class="btn btn-info" id="delete_multiple"
            >Delete Selected</button>

        <table class="table table-hover">
            <thead>
            <tr>
                <th>Check</th>
                <th>SL</th>
                <th>ID</th>
                <th>name</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            <!--        Here getAllBookData is a object -->
            <?php
            $serialNumber = 1;
            foreach ($getAllTrashedEmailData as $email) {
                ?>
                <tr>
                    <td><input type="checkbox" name="mark[]" value="<?php echo $email->id ?>"></td>
                    <td><?php echo $serialNumber++ ?></td>
                    <td><?php echo $email->id ?></td>
                    <td><?php echo $email->name ?></td>
                    <td><?php echo $email->email ?></td>
                    <td>
                        <a href="recover.php?id=<?php echo $email->id ?>" class="btn btn-info" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo $email->id ?>" class="btn btn-primary" role="button">Delete</a>
                    </td>
                </tr>

                <?php
            }
            ?>
            </tbody>
        </table>



    </form>

</div>

<script>

    // first er ta button id
    // then form id
    
   $('#delete_multiple').on('click', function () {
        document.forms[0].action = "delete_multiple.php";
       $('#multiple').submit();
   });
</script>

</body>
</html>

