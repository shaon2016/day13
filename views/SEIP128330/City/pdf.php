<?php

include_once("../../../vendor/autoload.php");

$city = new \App\BITM\SEIP128330\City\City();

$allData = $city->index();

$dataToShowInPDF = "";
$sl = 0;
foreach ($allData as $data):
    $sl++;

    $dataToShowInPDF .= "<tr>";
    $dataToShowInPDF .= "<td> $sl </td>";
    $dataToShowInPDF .= "<td> data->id</td>";
    $dataToShowInPDF .= "<td> $data->name </td>";
    $dataToShowInPDF .= "<td> $data->city </td>";
    $dataToShowInPDF .= "</tr>";

endforeach;

$html = <<<EDO

<table class="table table-hover">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Name</th>
            <th>Date</th>
        </tr>
        </thead>
        <tbody>
        
            $dataToShowInPDF;
        

        </tbody>
    </table>

EDO;

// Require composer autoload
require_once ('../../../vendor/mpdf/mpdf/mpdf.php');
//Create an instance of the class:

$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output('list.pdf', 'D');

?>