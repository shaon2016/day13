<?php

include_once('../../../vendor/autoload.php');
use App\BITM\SEIP128330\Utility;

$summeryEdit = new \App\BITM\SEIP128330\SummeryOfOrganizations\SummeryOfOrganizations();
$summeryEdit->prepareVariableValue($_GET);
$singleSummeryItem = $summeryEdit->view();
//
//Utility::dd($singleSummeryItem);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Email list</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--    for offline -->
    <link rel="stylesheet" type="text/css" href="../../../resource/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resource/js/bootstrap.js">
    <!--for online also works on ofline-->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Email name</h2>

    <form class="form-horizontal" role="form" action = "update.php" method="post">
        <div class="form-group">
            <label class="control-label col-sm-2">Edit name:</label>

            <div class="col-sm-10">
                <input type="hidden" id="id" name="id" value="<?php echo $singleSummeryItem['id']?>" />
                <input type="text" class="form-control" name = 'organization' id="organization" value="<?php echo $singleSummeryItem['organization']?>">
            </div>
        </div>


        <div class="form-group">
            <label class="control-label col-sm-2">Edit email:</label>

            <div class="col-sm-10">
                <input type="hidden" id="id" name="id" value="<?php echo $singleSummeryItem['id']?>" />
                <input type="text" class="form-control" name = 'summery' id="summery" value="<?php echo $singleSummeryItem['summery']?>">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Update</button>
            </div>
        </div>
    </form>
</div>

</body>
</html>

