<?php

include_once('../../../vendor/autoload.php');

$summeryView = new \App\BITM\SEIP128330\SummeryOfOrganizations\SummeryOfOrganizations();
$summeryView->prepareVariableValue($_GET);
$getSingleSummeryData = $summeryView->view();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Organization</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><?php echo $getSingleSummeryData['summery'] ?></h2>

    <table class="table table-hover">
        <thead>
        <tr>

            <th>ID</th>
            <th>Organization</th>
            <th>summery</th>

        </tr>
        </thead>
        <tbody>


        <tr>
            <td><?php echo $getSingleSummeryData['id'] ?></td>
            <td><?php echo $getSingleSummeryData['organization'] ?></td>
            <td><?php echo $getSingleSummeryData['summery'] ?></td>
        </tr>
        </tbody>
    </table>
</div>


</body>
</html>


