<?php
include_once('../../../vendor/autoload.php');

use App\BITM\SEIP128330\BookTitle\BookTitle;
use App\BITM\SEIP128330\Utility;
$book_title = new BookTitle();

$book_title->prepareVariableValue($_POST);
$book_title->store();

Utility::reDirectAPageIntoAnotherPage('index.php');
