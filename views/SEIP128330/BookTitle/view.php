<?php

include_once('../../../vendor/autoload.php');
use App\BITM\SEIP128330\BookTitle\BookTitle;

$bookView = new BookTitle();
$bookView->prepareVariableValue($_GET);
$getSingleBookData = $bookView->view();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Book Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><?php echo $getSingleBookData->title ?></h2>

    <table class="table table-hover">
        <thead>
        <tr>

            <th>ID</th>
            <th>Title</th>

        </tr>
        </thead>
        <tbody>


        <tr>
            <td><?php echo $getSingleBookData->id ?></td>
            <td><?php echo $getSingleBookData->title ?></td>
        </tr>
        </tbody>
    </table>
</div>


</body>
</html>


