<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 6/16/2016
 * Time: 3:02 PM
 */

namespace App\BITM\SEIP128330\Hobby;


use App\BITM\SEIP128330\Message;
use App\BITM\SEIP128330\Utility;

class Hobby
{
    private $id = "";
    private $hobby = "";
    private $con;
    private $allHobbyData = array();
    private $deleted_at;

    function __construct()
    {
        $this->con = mysqli_connect('localhost', 'root', "", 'atomicprojectb22');
    }

    // Accessing DB for list to show data
    function index()
    {
        $sql = "SELECT * FROM `hobby` where deleted_at is null";
        $result = mysqli_query($this->con, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in allhobbyData variable and return it to show the data
        // in index.php file
// we must declare allhobbyData as array to contain all data
        while ($row = mysqli_fetch_assoc($result))
            $this->allHobbyData[] = $row;

        return $this->allHobbyData;
    }

    function prepare($data)
    {
        if (array_key_exists('hobby', $data))
            $this->hobby = $data['hobby'];
        if(array_key_exists('id', $data))
            $this->id = $data['id'];


        //Utility::dd($comma_seperated_post_value);
    }

    function store()
    {
        $comma_seperated_post_value = implode(",", $this->hobby);
        $query = "INSERT INTO `hobby` (`hobby`) VALUES ('".$comma_seperated_post_value."')";
        $result = mysqli_query($this->con, $query);

        if($result) {

            Message::examineMessage("Inserted");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        }else {
            Message::examineMessage("Error");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        }
    }

    function view()
    {
        $query = "SELECT * FROM `hobby` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->con, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }

    function update()
    {
        $comma_seperated_post_value = implode(",", $this->hobby);

        
        $sql = "UPDATE `hobby` SET `hobby` = '".$comma_seperated_post_value."' WHERE `hobby`.`id` = ". $this->id;

        $result = mysqli_query($this->con, $sql);

        if($result) {

            Message::examineMessage("Update");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        }else  {
            Message::examineMessage("Error");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        }
    }
    
    function delete()
    {
        $sql = "delete from `hobby` where id = ". $this->id;
        
        $result = mysqli_query($this->con, $sql);

        if($result) {

            Message::examineMessage("Deleted");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        }else  {
            Message::examineMessage("Error");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        }
        
    }

    function trash()
    {
        $this->deleted_at = time();
        $sql = "UPDATE `hobby` SET deleted_at = '".$this->deleted_at."' WHERE `hobby`.`id` = ". $this->id;

        $result = mysqli_query($this->con, $sql);

        if($result) {
            Message::examineMessage("Trashed");
            Utility::reDirectAPageIntoAnotherPage("index.php");

        }else {
            Message::examineMessage("Error");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        }
    }

    function trashed()
    {
        $sql = "SELECT * FROM `hobby` where deleted_at is not null";
        $result = mysqli_query($this->con, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in allhobbyData variable and return it to show the data
        // in index.php file
// we must declare allhobbyData as array to contain all data
        while ($row = mysqli_fetch_assoc($result))
            $this->allHobbyData[] = $row;

        return $this->allHobbyData;
    }

    function recover()
    {
        $sql = "update `hobby` set deleted_at = NULL WHERE id =" . $this->id;
        $result = mysqli_query($this->con, $sql);

        if($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been recovered successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }
    
    function recoverSelected($ids = array())
    {
        if(is_array($ids) && count($ids) > 0) {
            $IDs = implode(",", $ids);

            $sql = "update `hobby` set deleted_at = NULL WHERE id IN (". $IDs .")";
            $result = mysqli_query($this->con, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been recovered successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    function deleteSelected($IDs = array())
    {
        if(is_array($IDs) && count($IDs) > 0) {
            $ids = implode(",", $IDs);

            $sql = 'delete from hobby where id IN ('.$ids.')';

            $result = mysqli_query($this->con, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been Deleted successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `hobby` WHERE deleted_at IS NULL";

        $result=mysqli_query($this->con,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allHobby = array();
        $query="SELECT * FROM `hobby` WHERE deleted_at is null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->con, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allHobby[] = $row;
        }

        return $_allHobby;

    }

}