<?php
session_start();
include_once("../../../vendor/autoload.php");
use App\BITM\SEIP128330\Education\Education;
use App\BITM\SEIP128330\Utility;
use App\BITM\SEIP128330\Message;

////Creating object
//$education = new Hobby();
////Getting DB data as object form
//$getAllEducationData = $education->index();
//// Checking DB data
////Utility::dd($getAllEducationData);

$education = new Education();

//Utility::d($allBook);

if (array_key_exists('itemPerPage', $_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
} else {
    $_SESSION['itemPerPage'] = 5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage = $_SESSION['itemPerPage'];
$totalItem = $education->count();


$totalPage = ceil($totalItem / $itemPerPage);
//Utility::dd($itemPerPage);
$pagination = "";


if (array_key_exists('pageNumber', $_GET)) {
    $pageNumber = $_GET['pageNumber'];
} else {
    $pageNumber = 1;
}
for ($i = 1; $i <= $totalPage; $i++) {
    $class = ($pageNumber == $i) ? "active" : "";
    $pagination .= "<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom = $itemPerPage * ($pageNumber - 1);
if (strtoupper($_SERVER['REQUEST_METHOD']) == "GET")
    $getAllEducationData = $education->paginator($pageStartFrom, $itemPerPage);
if (strtoupper($_SERVER['REQUEST_METHOD']) == "POST") {
    $getAllEducationData = $education->prepareVariableValue($_POST)->index();
}
if (strtoupper($_SERVER['REQUEST_METHOD']) == "GET" && isset($_GET['search'])) {
    $getAllEducationData = $education->prepareVariableValue($_GET)->index();
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Education</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Education Level List</h2>
    <a href="create.php" class="btn btn-info" role="button">Create Again</a>
    <a href="trashed.php" class="btn btn-info" role="button">Go to trashed</a>
    <a href="../Education/pdf.php" class="btn btn-info" role="button">Download as pdf</a>
    <a href="../Education/xl.php" class="btn btn-info" role="button">Download as excel</a>
    <a href="../Education/mail.php" class="btn btn-info" role="button">Send mail to friend</a>
    
    <div id='message'>
        <?php
        if (array_key_exists('message', $_SESSION) && (!empty($_SESSION['message'])))
            echo Message::examineMessage();
        ?>
    </div>

    <p></p>
    <form role="form" action="index.php" method="get">
        <label class="control-label col-sm-2">Search</label>

        <div class="col-sm-10">
            <label> <input type="text" class="form-control" name='search' id="search"
                           placeholder="Search name"></label>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
    <form role="form" action="index.php" method="post">


        <label class="control-label col-sm-2">Filter by name:</label>

        <div class="col-sm-10">
            <label> <input type="text" class="form-control" name='filterByName' id="filterByName"
                           placeholder="Filter by name"></label>
        </div>

        <label class="control-label col-sm-2">Filter by Level:</label>

        <div class="col-sm-10">
            <label> <input type="text" class="form-control" name='filterByLevel' id="filterByLevel"
                           placeholder="Filter by Level"></label>
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>


    <form role="form">
        <div class="form-group">
            <label for="sel1">Select homw many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option

                    <?php
                    if ($itemPerPage == 5) {

                        ?>
                        selected

                        <?php
                    }
                    ?>


                >5
                </option>
                <option
                    <?php
                    if ($itemPerPage == 10) {

                        ?>
                        selected

                        <?php
                    }
                    ?>
                >10
                </option>
                <option
                    <?php
                    if ($itemPerPage == 15) {

                        ?>
                        selected

                        <?php
                    }
                    ?>
                >15
                </option>
                <option
                    <?php
                    if ($itemPerPage == 20) {

                        ?>
                        selected

                        <?php
                    }
                    ?>
                >20
                </option>
                <option
                    <?php
                    if ($itemPerPage == 25) {

                        ?>
                        selected

                        <?php
                    }
                    ?>
                >25
                </option>
            </select>
            <button type="submit">Go!</button>

        </div>
    </form>

    <table class="table table-hover">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Name</th>
            <th>Level</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        <!--        Here getAllBookData is a object -->
        <?php
        $serialNumber = 1;
        foreach ($getAllEducationData as $education) {
            ?>
            <tr>
                <td><?php echo $serialNumber++ + $pageStartFrom ?></td>
                <td><?php echo $education['id'] ?></td>
                <td><?php echo $education['name'] ?></td>
                <td><?php echo $education['level'] ?></td>

                <td>
                    <a href="view.php?id=<?php echo $education['id'] ?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $education['id'] ?>" class="btn btn-primary" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $education['id'] ?>" class="btn btn-danger"
                       role="button">Delete</a>
                    <a href="trash.php?id=<?php echo $education['id'] ?>" class="btn btn-warning"
                       role="button">Trash</a>
                </td>
            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>
    <ul class="pagination">
        <li
            <?php
            if ($pageNumber == 1) {

                ?>
                class="hidden"

                <?php
            }
            ?>
        ><a href="index.php?pageNumber= <?php echo($pageNumber - 1) ?>>">Prev</a></li>
        <?php echo $pagination ?>
        <li
            <?php
            if ($pageNumber == $totalPage) {

                ?>
                class="hidden"

                <?php
            }
            ?>
        ><a href="index.php?pageNumber= <?php echo($pageNumber + 1) ?>>">Next</a></li>
    </ul>
</div>

<script>

    $('#message').show().delay(2000).fadeOut();
</script>

</body>
</html>

