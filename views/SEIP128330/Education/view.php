<?php

include_once('../../../vendor/autoload.php');

$educationView = new \App\BITM\SEIP128330\Education\Education();
$educationView->prepareVariableValue($_GET);
$getSingleEducationData = $educationView->view();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Education</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><?php echo $getSingleEducationData['name'] ?></h2>

    <table class="table table-hover">
        <thead>
        <tr>

            <th>ID</th>
            <th>Name</th>
            <th>Level</th>

        </tr>
        </thead>
        <tbody>


        <tr>
            <td><?php echo $getSingleEducationData['id'] ?></td>
            <td><?php echo $getSingleEducationData['name'] ?></td>
            <td><?php echo $getSingleEducationData['level'] ?></td>
        </tr>
        </tbody>
    </table>
</div>


</body>
</html>


