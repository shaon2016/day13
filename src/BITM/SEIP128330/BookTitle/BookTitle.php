<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 6/5/2016
 * Time: 6:38 PM
 */

namespace App\BITM\SEIP128330\BookTitle;


use App\BITM\SEIP128330\Message;
use App\BITM\SEIP128330\Utility;

class BookTitle
{
    public $id;
    private $title;
    private $deletedAt;
    private $email;
    private $con;
    private $filterByTitle;
    private $filterByDescription;
    private $search;
    private $description;
    private $allbookData = array();
    private $allTrashedBookData = array();

    function __construct()
    {
        $this->con = mysqli_connect('localhost', 'root', "", 'atomicprojectb22');
    }

    // Accessing DB for list to show data
    function index()
    {
        $whereClause= " 1=1 ";
        if(!empty($this->filterByTitle)){
            $whereClause.="AND title LIKE '%".$this->filterByTitle."%'";
        }
        if(!empty($this->filterByDescription)){
            $whereClause.="AND description LIKE '%".$this->filterByDescription."%'";
        }
        if(!empty($this->search)){
            $whereClause.="AND description LIKE '%".$this->search."%' OR title LIKE '%".$this->search."%'";
        }

        $sql = "SELECT * FROM `book` where deleted_at is null AND ". $whereClause;
        $result = mysqli_query($this->con, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in allBookData variable and return it to show the data
        // in index.php file
// we must declare allBookData as array to contain all data
        while ($row = mysqli_fetch_object($result))
            $this->allbookData[] = $row;

        return $this->allbookData;
    }

    public function create()
    {
        return " I am create form ";

    }

    public function prepareVariableValue($data)
    {
        if (array_key_exists('email', $data))
            $this->email = $data['email'];

        if (array_key_exists('title', $data))
            $this->title = $data['title'];

        if (array_key_exists('id', $data))
            $this->id = $data['id'];

        if (array_key_exists('description', $data))
            $this->description = ($data['description']);

        if (array_key_exists('search', $data))
            $this->search = ($data['search']);

        if (array_key_exists('filterBydescription', $data))
            $this->filterByDescription = ($data['filterBydescription']);

        if (array_key_exists('filterByTitle', $data))
            $this->filterByTitle = ($data['filterByTitle']);

        return $this;
    }

    function store()
    {
        $result = mysqli_query($this->con, "INSERT INTO 
`atomicprojectb22`.`book` ( `title`, `description`, `email`)
VALUES ('" . $this->title . "', '".$this->description."', '" . $this->email . "')");


        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been stored successfully!</strong>
            </div>");
        } else Message::examineMessage("error");

        //echo $result;
        // echo $this->title;

        //return " I am storing data ";
    }

    function edit()
    {
        return "I am editing form ";

    }

    function update()
    {
        $query = "UPDATE `book` SET `title` = '" . $this->title . "' 
        WHERE `book`.`id` = " . $this->id;
        $result = mysqli_query($this->con, $query);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been Updated successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function delete()
    {
        $query = "DELETE FROM `atomicprojectb22`.`book` WHERE `book`.`id` =" . $this->id;

        $result = mysqli_query($this->con, $query);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been deleted successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function view()
    {
        $query = "SELECT * FROM `book` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->con, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    function trash()
    {
        $this->deletedAt = time();

        $sql = "UPDATE `book` SET `deleted_at` = '".$this->deletedAt."' WHERE `book`.`id` =". $this->id;
        $result = mysqli_query($this->con, $sql);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been trashed successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function trashed()
    {
        $sql = "SELECT * FROM `book` where deleted_at is not null";
        $result = mysqli_query($this->con, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in allBookData variable and return it to show the data
        // in index.php file
// we must declare allBookData as array to contain all data
        while ($row = mysqli_fetch_object($result))
            $this->allTrashedBookData[] = $row;

        return $this->allTrashedBookData;
    }

    function recover ()
    {
        $sql = "update book set deleted_at = null where id = ". $this->id;
        $result = mysqli_query($this->con, $sql);

        if($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been recovered successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function recoverSelected($ids = array())
    {
        if(is_array($ids) && count($ids) > 0) {
            $IDs = implode(",", $ids);

            $sql = "update book set deleted_at = NULL WHERE id IN (". $IDs .")";
            $result = mysqli_query($this->con, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been recovered successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    function deleteSelected($IDs = array())
    {
        if(is_array($IDs) && count($IDs) > 0) {
            $ids = implode(",", $IDs);

            $sql = 'delete from `book` where id IN ('.$ids.')';

            $result = mysqli_query($this->con, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been Deleted successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `book` WHERE deleted_at IS NULL";
        $result=mysqli_query($this->con,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allBook = array();
        $query="SELECT * FROM `book` WHERE deleted_at is null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->con, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allBook[] = $row;
        }

        return $_allBook;

    }
}