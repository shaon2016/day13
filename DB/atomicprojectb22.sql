-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2016 at 02:29 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomicprojectb22`
--

-- --------------------------------------------------------

--
-- Table structure for table `birth`
--

CREATE TABLE `birth` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `date` date NOT NULL,
  `email` varchar(150) NOT NULL,
  `deleted_at` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birth`
--

INSERT INTO `birth` (`id`, `name`, `date`, `email`, `deleted_at`) VALUES
(6, 'asd', '0123-12-12', '', '1467190629'),
(7, 'shaon', '0233-12-12', '', NULL),
(8, 'dsa', '2113-02-12', '', NULL),
(9, 'nadim', '2016-06-22', '', NULL),
(10, 'dsa', '2016-06-17', '', NULL),
(11, 'ashiq', '2016-06-18', '', NULL),
(12, 'ashiqul islam', '2016-06-25', '', NULL),
(13, 'shaon', '2016-07-07', 'ashiq201@yahoo.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `email` varchar(150) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `description`, `email`, `deleted_at`) VALUES
(11, 'hello', 'dsa', '', '1467226564'),
(12, 'hello', '', '', '1466707378'),
(14, 'data added', 'dsa', '', '1467226566'),
(16, 'hello', 'ewq', '', NULL),
(17, 'hello', 'dsa', '', NULL),
(18, 'hello again', '', '', '1467226567'),
(19, 'hello', 'qwe', '', NULL),
(20, 'hello', '', '', NULL),
(22, 'hello', 'asd', '', NULL),
(23, 'physics', '', '', NULL),
(24, 'physics I', 'asd', '', NULL),
(25, 'lol', 'qwe', '', NULL),
(26, 'physics', '<p>asdas</p>\r\n<p>a</p>\r\n<p>sd</p>\r\n<p>asd</p>\r\n<p>&nbsp;</p>', 'ashiq201@yahoo.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `city` varchar(150) NOT NULL,
  `deleted_at` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `deleted_at`) VALUES
(1, 'shaon', 'Chittagong', NULL),
(2, 'dsa', 'Chittagong', NULL),
(4, 'asd', 'Rajshahi', NULL),
(5, 'dsa', 'Chittagong', NULL),
(6, 'dsa', 'Dhaka', NULL),
(7, 'dsa', 'Dhaka', NULL),
(8, 'dsa', 'Dhaka', NULL),
(9, 'dsa', 'Sylhet', NULL),
(10, 'dsa', 'Dhaka', NULL),
(11, 'dsa', 'Barishal', NULL),
(12, 'dsa', 'Dhaka', '1467131882');

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE `education` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `level` varchar(150) NOT NULL,
  `deleted_at` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`id`, `name`, `level`, `deleted_at`) VALUES
(4, 'asd', 'BSC in CSE', '1467193538'),
(5, 'dsa', 'BSC in Architecture', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `deleted_at` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `deleted_at`) VALUES
(4, 'asd', 'ashiq.desire@gmail.com', NULL),
(5, 'asd', 'ashiq.desire@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE `hobby` (
  `id` int(11) NOT NULL,
  `hobby` varchar(150) NOT NULL,
  `deleted_at` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `hobby`, `deleted_at`) VALUES
(1, 'Gardening', '1467226321'),
(2, 'Coding', '1467226321'),
(3, 'Playing Football', '1467226322'),
(4, 'Gardening,Cricket', NULL),
(5, 'Gardening', NULL),
(6, 'Cricket,Swimming', NULL),
(7, 'Playing Football,Swimming', NULL),
(8, 'Playing Football,Coding', NULL),
(9, 'Playing Football', NULL),
(10, 'Cricket', NULL),
(11, 'Gardening,Playing Football,Swimming', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE `profilepicture` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `images` varchar(150) NOT NULL,
  `deleted_at` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`id`, `name`, `images`, `deleted_at`) VALUES
(3, 'asd', '1467042494Screenshot (45).png', NULL),
(4, 'asd', '146813934913599907_10205011167717564_6566911026928559948_n.jpg', NULL),
(5, 'dsa', '1467097012download (2).jpg', NULL),
(6, 'asdasd', '1467097019download (3).jpg', NULL),
(7, 'qwe', '1467097027download.jpg', NULL),
(8, 'dsa', '1467097035images.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `summery`
--

CREATE TABLE `summery` (
  `id` int(11) NOT NULL,
  `organization` varchar(150) NOT NULL,
  `summery` text NOT NULL,
  `deleted_at` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summery`
--

INSERT INTO `summery` (`id`, `organization`, `summery`, `deleted_at`) VALUES
(9, 'asd', 'asd', NULL),
(10, 'dsa', 'dsa', '1467136961'),
(11, 'BITM', 'Coaching ', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birth`
--
ALTER TABLE `birth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `education`
--
ALTER TABLE `education`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepicture`
--
ALTER TABLE `profilepicture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summery`
--
ALTER TABLE `summery`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birth`
--
ALTER TABLE `birth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `education`
--
ALTER TABLE `education`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `profilepicture`
--
ALTER TABLE `profilepicture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `summery`
--
ALTER TABLE `summery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
