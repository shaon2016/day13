<?php

include_once('../../../vendor/autoload.php');
use App\BITM\SEIP128330\Utility;

$hobbyedit = new \App\BITM\SEIP128330\Hobby\Hobby();
$hobbyedit->prepare($_GET);
$singleHobbyItem = $hobbyedit->view();

////Utility::dd($_GET);
//Utility::dd($singleHobbyItem);

/*
 * object(stdClass)#5 (2) {
  ["id"]=>
  string(1) "3"
  ["hobby"]=>
  string(16) "Cricket,Swimming"
}
 */


$convertingDBValueArrayToStringToShowInCheckBox = explode(",", $singleHobbyItem['hobby']);


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select your hobby</h2>

    <form role="form" method="post" action="update.php">

        <input type="hidden" name="id" value="<?php echo $singleHobbyItem['id']?>">
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Gardening"
                    <?php if (in_array("Gardening", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        checked
                        <?php
                    }
                    ?>
                >Gardening</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[]
                          value="Playing Football"
                    <?php if (in_array("Playing Football", $convertingDBValueArrayToStringToShowInCheckBox)) {
                    ?>
                    checked
                    <?php
                }
                ?>
                >Playing Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Coding"
                    <?php if (in_array("Coding", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        checked
                        <?php
                    }
                    ?>

                >Coding</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Cricket"
                    <?php if (in_array("Cricket", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        checked
                        <?php
                    }
                    ?>
                >Cricket</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Swimming"
                    <?php if (in_array("Swimming", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        checked
                        <?php
                    }
                    ?>
                >Swimming</label>
        </div>
        <input type="submit" value="Update">
    </form>
</div>

</body>
</html>

