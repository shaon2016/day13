<!--include_once('../../../vendor/autoload.php');-->
<!---->
<!--function __autoload($className){-->
<!--    include_once('../../../'.$className);-->
<!---->
<!--}-->
<!---->
<!--include_once('../../../src/BITM/SEIP128330/BookTitle/BookTitle.php');-->
<!---->
<!--use App\BITM\SEIP128330\BookTitle\BookTitle;-->


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<!--    for offline -->
    <link rel="stylesheet" type="text/css" href="../../../resource/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resource/js/bootstrap.js">
<!--for online also works on ofline-->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
</head>
<body>

<div class="container">
    <h2>Creating Book title</h2>

    <form class="form-horizontal" role="form" action = "store.php" method="post">
        <div class="form-group">
            <label class="control-label col-sm-2">Enter book title:</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" name = 'title' id="title" placeholder="Enter Book Title">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2">Enter book Description:</label>

            <div class="col-sm-10">
                <textarea class="form-control" name = 'description' id="description" placeholder="Enter Book Description"></textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2">Enter email:</label>

            <div class="col-sm-10">
                <input type="email" class="form-control" name='email' id="name" placeholder="Enter email">
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </form>
</div>

</body>
<script>
    tinymce.init({
        selector: '#description'
    });
</script>
</html>


<!--<head>-->
<!--<link rel="stylesheet" type="text/css" href="../../../resource/css/bootstrap.css">-->
<!--</head>-->
<!---->
<!--<head>-->
<!--    <link rel="stylesheet" type="text/css" href="../../../resource/js/bootstrap.js">-->
<!--</head>-->

