<?php
/**
 * Created by PhpStorm.
 * User: Shaon
 * Date: 6/28/2016
 * Time: 11:33 PM
 */

namespace App\BITM\SEIP128330\SummeryOfOrganizations;


include_once('../../../vendor/autoload.php');

use App\BITM\SEIP128330\Message;
use App\BITM\SEIP128330\Utility;


class SummeryOfOrganizations
{
    public $id;
    private $summery;
    private $organization;
    private $deletedAt;
    private $con;
    private $allsummeryData = array();
    private $allTrashedsummeryData = array();

    function __construct()
    {
        $this->con = mysqli_connect('localhost', 'root', "", 'atomicprojectb22');
    }

    // Accessing DB for list to show data
    function index()
    {
        $sql = "SELECT * FROM `summery` where deleted_at is null";
        $result = mysqli_query($this->con, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in allsummeryData variable and return it to show the data
        // in index.php file
// we must declare allsummeryData as array to contain all data
        while ($row = mysqli_fetch_object($result))
            $this->allsummeryData[] = $row;

        return $this->allsummeryData;
    }


    public function prepareVariableValue($data)
    {
        if (array_key_exists('summery', $data))
            $this->summery = $data['summery'];


        if (array_key_exists('organization', $data))
            $this->organization = $data['organization'];

        if (array_key_exists('id', $data))
            $this->id = $data['id'];
    }

    function store()
    {

        $sql = "INSERT INTO `atomicprojectb22`.`summery` (`organization`, `summery`)
 VALUES ('".$this->organization."', '".$this->summery."')";

        $result = mysqli_query($this->con, $sql);


        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been stored successfully!</strong>
            </div>");

            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has not been stored successfully!</strong>
            </div>");


            Utility::reDirectAPageIntoAnotherPage("index.php");
        }

        //echo $result;
        // echo $this->summery;

        //return " I am storing data ";
    }


    function update()
    {
        $query = "UPDATE `summery` SET `organization` = '" . $this->organization . "', `summery` = '" . $this->summery . "'
        WHERE `summery`.`id` = " . $this->id;
        $result = mysqli_query($this->con, $query);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been Updated successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function delete()
    {
        $query = "DELETE FROM `atomicprojectb22`.`summery` WHERE `summery`.`id` =" . $this->id;

        $result = mysqli_query($this->con, $query);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been deleted successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function view()
    {
        $query = "SELECT * FROM `summery` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->con, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }

    function trash()
    {
        $this->deletedAt = time();

        $sql = "UPDATE `summery` SET `deleted_at` = '".$this->deletedAt."' WHERE `summery`.`id` =". $this->id;
        $result = mysqli_query($this->con, $sql);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been trashed successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function trashed()
    {
        $sql = "SELECT * FROM `summery` where deleted_at is not null";
        $result = mysqli_query($this->con, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in allsummeryData variable and return it to show the data
        // in index.php file
// we must declare allsummeryData as array to contain all data
        while ($row = mysqli_fetch_assoc($result))
            $this->allTrashedsummeryData[] = $row;

        return $this->allTrashedsummeryData;
    }

    function recover ()
    {
        $sql = "update summery set deleted_at = null where id = ". $this->id;
        $result = mysqli_query($this->con, $sql);

        if($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been recovered successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function recoverSelected($ids = array())
    {
        if(is_array($ids) && count($ids) > 0) {
            $IDs = implode(",", $ids);

            $sql = "update summery set deleted_at = NULL WHERE id IN (". $IDs .")";
            $result = mysqli_query($this->con, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been recovered successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    function deleteSelected($IDs = array())
    {
        if(is_array($IDs) && count($IDs) > 0) {
            $ids = implode(",", $IDs);

            $sql = 'delete from `summery` where id IN ('.$ids.')';

            $result = mysqli_query($this->con, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been Deleted successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `summery` WHERE deleted_at IS NULL";
        $result=mysqli_query($this->con,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allsummery = array();
        $query="SELECT * FROM `summery` WHERE deleted_at is null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->con, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allsummery[] = $row;
        }

        return $_allsummery;

    }
}