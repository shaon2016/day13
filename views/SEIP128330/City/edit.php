<?php

include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP128330\Utility;

$cityViewForEdit = new \App\BITM\SEIP128330\City\City();
$cityViewForEdit->prepareVariableValue($_GET);
$singleCityItem = $cityViewForEdit->view();

$convertingDBValueArrayToStringToShowInCheckBox = explode(",", $singleCityItem->city);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit City list</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--    for offline -->
    <link rel="stylesheet" type="text/css" href="../../../resource/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resource/js/bootstrap.js">
    <!--for online also works on ofline-->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit City name</h2>

    <form class="form-horizontal" role="form" action = "update.php" method="post">
        <div class="form-group">
            <label class="control-label col-sm-2">Edit book title:</label>

            <div class="col-sm-10">
                <input type="hidden" id="id" name="id" value="<?php echo $singleCityItem->id?>" />
                <input type="text" class="form-control" name = 'name' id="name" value="<?php echo $singleCityItem->name?>">
            </div>
        </div>
        <div class="form-horizontal">
            <label>City (Select one):</label>
            <select class="form-control" id="city" name="city">
                <option
                   
                    <?php
                     if (in_array("Dhaka", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        selected

                        <?php
                    }
                    ?>

                >Dhaka
                </option>
                <option
                    <?php
                    if (in_array("Chittagong", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        selected

                        <?php
                    }
                    ?>

                >Chittagong
                </option>
                <option
                    <?php
                    if (in_array("Rajshahi", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        selected

                        <?php
                    }
                    ?>

                >Rajshahi
                </option>
                <option
                    <?php
                    if (in_array("Barishal", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        selected

                        <?php
                    }
                    ?>

                >Barishal
                </option>
                <option
                    <?php
                    if (in_array("Sylhet", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        selected

                        <?php
                    }
                    ?>

                >Sylhet
                </option>
            </select>

        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Update</button>
            </div>
        </div>
    </form>
</div>

</body>
</html>

