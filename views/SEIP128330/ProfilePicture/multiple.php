<?php

include "../../../vendor/autoload.php";

$propic = new \App\BITM\SEIP128330\ProfilePicture\ImageUploader();



if(array_key_exists('recoverAll', $_POST)) {
    $propic->recoverSelected($_POST['id']);
}


if(array_key_exists('deleteAll', $_POST)) {
    $propic->deleteSelected($_POST['id']);
}