<?php

include "../../../vendor/autoload.php";

$birth = new \App\BITM\SEIP128330\Birth\Birth();


if(array_key_exists('recoverAll', $_POST)) {
    $birth->recoverSelected($_POST['id']);
}


if(array_key_exists('deleteAll', $_POST)) {
    $birth->deleteSelected($_POST['id']);
}