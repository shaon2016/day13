<?php
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP128330\ProfilePicture\ImageUploader;
use App\Bitm\SEIP128330\Utility;
use App\Bitm\SEIP128330\Message;

$profile_picture= new ImageUploader();

$profile_picture->prepare($_GET);

$singleInfo=$profile_picture->view();
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
    <h2>Single Info List</h2>

    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>

                <th>ID</th>
                <th>Name</th>
                <th>Image</th>

            </tr>
            </thead>
            <tbody>
            <tr>

                <td><?php echo $singleInfo->id?></td>
                <td><?php echo $singleInfo->name?></td>
                <td><img src="../../../Resources/Images/<?php echo $singleInfo->images ?>" alt="image" height="300px" width="300px" class="img-responsive"> </td>


            </tr>


            </tbody>
        </table>
    </div>
</div>
