<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 6/5/2016
 * Time: 6:38 PM
 */

namespace App\BITM\SEIP128330\City;


use App\BITM\SEIP128330\Message;
use App\BITM\SEIP128330\Utility;

class City
{
    public $id;
    private $city;
    private $name;
    private $deletedAt;
    private $filterByName;
    private $filterByCity;
    private $search;
    private $con;
    private $allcityData = array();
    private $allTrashedcityData = array();

    function __construct()
    {
        $this->con = mysqli_connect('localhost', 'root', "", 'atomicprojectb22');
    }

    // Accessing DB for list to show data
    function index()
    {
        $whereClause= " 1=1 ";
        if(!empty($this->filterByName)){
            $whereClause.="AND name LIKE '%".$this->filterByName."%'";
        }
        if(!empty($this->filterByCity)){
            $whereClause.="AND city LIKE '%".$this->filterByCity."%'";
        }
        if(!empty($this->search)){
            $whereClause.="AND city LIKE '%".$this->search."%' OR name LIKE '%".$this->search."%'";
        }
        
        $sql = "SELECT * FROM `city` where deleted_at is null AND ". $whereClause;
        $result = mysqli_query($this->con, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in allcityData variable and return it to show the data
        // in index.php file
// we must declare allcityData as array to contain all data
        while ($row = mysqli_fetch_object($result))
            $this->allcityData[] = $row;

        return $this->allcityData;
    }


    public function prepareVariableValue($data)
    {
        if (array_key_exists('city', $data))
            $this->city = $data['city'];


        if (array_key_exists('name', $data))
            $this->name = $data['name'];

        if (array_key_exists('id', $data))
            $this->id = $data['id'];
        if (array_key_exists('description', $data))
            $this->description = ($data['description']);

        if (array_key_exists('search', $data))
            $this->search = ($data['search']);

        if (array_key_exists('filterByCity', $data))
            $this->filterByCity = ($data['filterByCity']);

        if (array_key_exists('filterByName', $data))
            $this->filterByName = ($data['filterByName']);

        return $this;
    }

    function store()
    {

        $sql = "INSERT INTO `atomicprojectb22`.`city` (`name`, `city`)
 VALUES ('".$this->name."', '".$this->city."')";

        $result = mysqli_query($this->con, $sql);


        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been stored successfully!</strong>
            </div>");

            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has not been stored successfully!</strong>
            </div>");


            Utility::reDirectAPageIntoAnotherPage("index.php");
        }

        //echo $result;
        // echo $this->city;

        //return " I am storing data ";
    }


    function update()
    {
        $query = "UPDATE `city` SET `name` = '" . $this->name . "', `city` = '" . $this->city . "'
        WHERE `city`.`id` = " . $this->id;
        $result = mysqli_query($this->con, $query);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been Updated successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function delete()
    {
        $query = "DELETE FROM `atomicprojectb22`.`city` WHERE `city`.`id` =" . $this->id;

        $result = mysqli_query($this->con, $query);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been deleted successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function view()
    {
        $query = "SELECT * FROM `city` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->con, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    function trash()
    {
        $this->deletedAt = time();

        $sql = "UPDATE `city` SET `deleted_at` = '".$this->deletedAt."' WHERE `city`.`id` =". $this->id;
        $result = mysqli_query($this->con, $sql);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been trashed successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function trashed()
    {
        $sql = "SELECT * FROM `city` where deleted_at is not null";
        $result = mysqli_query($this->con, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in allcityData variable and return it to show the data
        // in index.php file
// we must declare allcityData as array to contain all data
        while ($row = mysqli_fetch_object($result))
            $this->allTrashedcityData[] = $row;

        return $this->allTrashedcityData;
    }

    function recover ()
    {
        $sql = "update city set deleted_at = null where id = ". $this->id;
        $result = mysqli_query($this->con, $sql);

        if($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been recovered successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function recoverSelected($ids = array())
    {
        if(is_array($ids) && count($ids) > 0) {
            $IDs = implode(",", $ids);

            $sql = "update city set deleted_at = NULL WHERE id IN (". $IDs .")";
            $result = mysqli_query($this->con, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been recovered successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    function deleteSelected($IDs = array())
    {
        if(is_array($IDs) && count($IDs) > 0) {
            $ids = implode(",", $IDs);

            $sql = 'delete from `city` where id IN ('.$ids.')';

            $result = mysqli_query($this->con, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been Deleted successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `city` WHERE deleted_at IS NULL";
        $result=mysqli_query($this->con,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allcity = array();
        $query="SELECT * FROM `city` WHERE deleted_at is null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->con, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allcity[] = $row;
        }

        return $_allcity;

    }
}