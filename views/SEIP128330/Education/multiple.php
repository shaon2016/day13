<?php

include "../../../vendor/autoload.php";

$education = new \App\BITM\SEIP128330\Education\Education();



if(array_key_exists('recoverAll', $_POST)) {
    $education->recoverSelected($_POST['id']);
}


if(array_key_exists('deleteAll', $_POST)) {
    $education->deleteSelected($_POST['id']);
}