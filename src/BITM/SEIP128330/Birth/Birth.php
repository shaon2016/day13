<?php
/**
 * Created by PhpStorm.
 * User: Shaon
 * Date: 6/29/2016
 * Time: 2:20 PM
 */

namespace App\BITM\SEIP128330\Birth;
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP128330\Message;
use App\BITM\SEIP128330\Utility;
class Birth
{
    public $id;
    private $date;
    private $name;
    private $email;
    private $deletedAt;
    private $filterByName;
    private $filterByDate;
    private $search;
    private $con;
    private $allbirthData = array();
    private $allTrashedbirthData = array();

    function __construct()
    {
        $this->con = mysqli_connect('localhost', 'root', "", 'atomicprojectb22');
    }

    // Accessing DB for list to show data
    function index()
    {
        $whereClause= " 1=1 ";
        if(!empty($this->filterByName)){
            $whereClause.="AND name LIKE '%".$this->filterByName."%'";
        }
        if(!empty($this->filterByDate)){
            $whereClause.="AND date LIKE '%".$this->filterByDate."%'";
        }
        if(!empty($this->search)){
            $whereClause.="AND date LIKE '%".$this->search."%' OR name LIKE '%".$this->search."%'";
        }
        
        $sql = "SELECT * FROM `birth` where deleted_at is null AND ". $whereClause;
        $result = mysqli_query($this->con, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in allbirthData variable and return it to show the data
        // in index.php file
// we must declare allbirthData as array to contain all data
        while ($row = mysqli_fetch_assoc($result))
            $this->allbirthData[] = $row;

        return $this->allbirthData;
    }


    public function prepareVariableValue($data)
    {
        if (array_key_exists('date', $data))
            $this->date = $data['date'];


        if (array_key_exists('name', $data))
            $this->name = $data['name'];

        if (array_key_exists('id', $data))
            $this->id = $data['id'];

        if (array_key_exists('email', $data))
            $this->email = $data['email'];


        if (array_key_exists('search', $data))
            $this->search = ($data['search']);

        if (array_key_exists('filterByDate', $data))
            $this->filterByDate = ($data['filterByDate']);

        if (array_key_exists('filterByName', $data))
            $this->filterByName = ($data['filterByName']);

        return $this;
    }

    function store()
    {

        $sql = "INSERT INTO `atomicprojectb22`.`birth` (`name`, `date`, `email`)
 VALUES ('".$this->name."', '".$this->date."', '".$this->email."')";

        $result = mysqli_query($this->con, $sql);


        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been stored successfully!</strong>
            </div>");

            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has not been stored successfully!</strong>
            </div>");


            Utility::reDirectAPageIntoAnotherPage("index.php");
        }

        //echo $result;
        // echo $this->birth;

        //return " I am storing data ";
    }


    function update()
    {
        $query = "UPDATE `birth` SET `name` = '" . $this->name . "', `date` = '" . $this->date . "'
        WHERE `birth`.`id` = " . $this->id;
        $result = mysqli_query($this->con, $query);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been Updated successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function delete()
    {
        $query = "DELETE FROM `atomicprojectb22`.`birth` WHERE `birth`.`id` =" . $this->id;

        $result = mysqli_query($this->con, $query);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been deleted successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function view()
    {
        $query = "SELECT * FROM `birth` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->con, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }

    function trash()
    {
        $this->deletedAt = time();

        $sql = "UPDATE `birth` SET `deleted_at` = '".$this->deletedAt."' WHERE `birth`.`id` =". $this->id;
        $result = mysqli_query($this->con, $sql);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been trashed successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function trashed()
    {
        $sql = "SELECT * FROM `birth` where deleted_at is not null";
        $result = mysqli_query($this->con, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in allbirthData variable and return it to show the data
        // in index.php file
// we must declare allbirthData as array to contain all data
        while ($row = mysqli_fetch_assoc($result))
            $this->allTrashedbirthData[] = $row;

        return $this->allTrashedbirthData;
    }

    function recover ()
    {
        $sql = "update birth set deleted_at = null where id = ". $this->id;
        $result = mysqli_query($this->con, $sql);

        if($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been recovered successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function recoverSelected($ids = array())
    {
        if(is_array($ids) && count($ids) > 0) {
            $IDs = implode(",", $ids);

            $sql = "update birth set deleted_at = NULL WHERE id IN (". $IDs .")";
            $result = mysqli_query($this->con, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been recovered successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    function deleteSelected($IDs = array())
    {
        if(is_array($IDs) && count($IDs) > 0) {
            $ids = implode(",", $IDs);

            $sql = 'delete from `birth` where id IN ('.$ids.')';

            $result = mysqli_query($this->con, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been Deleted successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `birth` WHERE deleted_at IS NULL";
        $result=mysqli_query($this->con,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_allbirth = array();
        $query="SELECT * FROM `birth` WHERE deleted_at is null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->con, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_allbirth[] = $row;
        }

        return $_allbirth;

    }
}