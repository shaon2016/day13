<?php

include_once ('../../../vendor/autoload.php');

use App\BITM\SEIP128330\Utility;
use App\BITM\SEIP128330\Hobby\Hobby;

$education = new \App\BITM\SEIP128330\Education\Education();
$education->prepareVariableValue($_POST);
$education->store();
?>