<?php
session_start();
include_once("../../../vendor/autoload.php");
use App\BITM\SEIP128330\Utility;
use App\BITM\SEIP128330\Message;

//Creating object
$summery = new \App\BITM\SEIP128330\SummeryOfOrganizations\SummeryOfOrganizations();
//Getting DB data as object form
$getAllSummeryData = $summery->trashed();
// Checking DB data
//Utility::dd($getAllSummeryData);



?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Summery</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Organization's List</h2>

    <form action="multiple.php" method="post">

        <a href="index.php" class="btn btn-info" role="button">Go to index page</a>
        <button name = "recoverAll" class="btn btn-warning" role="button">Recover All</button>
        <button name = "deleteAll" class="btn btn-danger" role="button">Delete All</button>
        
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Check</th>
                <th>ID</th>
                <th>Organization</th>
                <th>Summery</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            <!--        Here getAllBookData is a object -->
            <?php
            $serialNumber = 1;
            foreach ($getAllSummeryData as $summery) {
                ?>
                <tr>
                    <td><input type="checkbox" name="id[]" value="<?php echo $summery['id'] ?>"></td>
                    <td><?php echo $summery['id'] ?></td>
                    <td><?php echo $summery['organization'] ?></td>
                    <td><?php echo $summery['summery'] ?></td>
                    <td>
                        <a href="recover.php?id=<?php echo $summery['id']?>" class="btn btn-info" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo $summery['id']?>" class="btn btn-primary" role="button">Delete</a>

                    </td>
                </tr>

                <?php
            }
            ?>
            </tbody>
        </table>

    </form>

</div>



</body>
</html>

