<?php

include_once('../../../vendor/autoload.php');
use App\BITM\SEIP128330\Utility;

$educationEdit = new \App\BITM\SEIP128330\Education\Education();
$educationEdit->prepareVariableValue($_GET);
$singleEducationItem = $educationEdit->view();


$convertingDBValueArrayToStringToShowInCheckBox = explode(",", $singleEducationItem['level']);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Education level</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">


    <form role="form" method="post" action="update.php">

        <h2>Your Name</h2>
        <div class="form-group">

            <div class="col-sm-10">
                <input type="text" class="form-control" name='name' id="name" value="<?php echo $singleEducationItem['name']?>">
            </div>
        </div>

        <br>


        <h2>Select your Education Level</h2>


        <input type="hidden" name="id" value="<?php echo $singleEducationItem['id'] ?>">

        <div class="radio">
            <label><input type="radio" name="level" value="SSC"
                    <?php if (in_array("SSC", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        checked
                        <?php
                    }
                    ?>
                > SSC</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="level" value="HSC"
                    <?php if (in_array("HSC", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        checked
                        <?php
                    }
                    ?>
                > HSC</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="level" value="BSC in CSE"

                    <?php if (in_array("BSC in CSE", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        checked
                        <?php
                    }
                    ?>

                > BSC in CSE</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="level" value="BSC in EEE"
                    <?php if (in_array("BSC in EEE", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        checked
                        <?php
                    }
                    ?>

                > BSC in EEE</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="level" value="BSC in ETE"

                    <?php if (in_array("BSC in ETE", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        checked
                        <?php
                    }
                    ?>

                > BSC in ETE</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="level" value="BSC in Civil"


                    <?php if (in_array("BSC in Civil", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        checked
                        <?php
                    }
                    ?>

                > BSC in Civil</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="level" value="BSC in Architecture"

                    <?php if (in_array("BSC in Architecture", $convertingDBValueArrayToStringToShowInCheckBox)) {
                        ?>
                        checked
                        <?php
                    }
                    ?>


                > BSC in Architecture</label>
        </div>
        <input type="submit" value="Update">
    </form>
</div>

</body>
</html>

