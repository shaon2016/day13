<?php

include_once('../../../vendor/autoload.php');

$birthView = new \App\BITM\SEIP128330\Birth\Birth();
$birthView->prepareVariableValue($_GET);
$getSingleBirthData = $birthView->view();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Organization</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><?php echo $getSingleBirthData['name'] ?></h2>

    <table class="table table-hover">
        <thead>
        <tr>

            <th>ID</th>
            <th>name</th>
            <th>date</th>
            <th>email</th>

        </tr>
        </thead>
        <tbody>


        <tr>
            <td><?php echo $getSingleBirthData['id'] ?></td>
            <td><?php echo $getSingleBirthData['name'] ?></td>
            <td><?php

                $contain = explode("-", $getSingleBirthData['date']);

                $date = $contain[2]. "-" . $contain[1] . "-" . $contain[0];

                echo  $date?></td>
            <td><?php echo $getSingleBirthData['email'] ?></td>
        </tr>
        </tbody>
    </table>
</div>


</body>
</html>


