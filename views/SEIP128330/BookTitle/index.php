<?php
session_start();
include_once("../../../vendor/autoload.php");
use App\BITM\SEIP128330\BookTitle\BookTitle;
use App\BITM\SEIP128330\Utility;
use App\BITM\SEIP128330\Message;

$book = new BookTitle();

//Utility::dd($_POST);

/*
 * checking item per page in session and global method get.
 * if not we set default value
 */

// How many item will be in per page
// we determine it from here

if (array_key_exists('itemPerPage', $_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
} else {
    $_SESSION['itemPerPage'] = 5;
}

//Utility::dd($_SESSION['itemPerPage']);

// First we determine
// 1) how many item will be in per page
// 2) How many page will be in this index page
// 3) we get total item using count method
// 4) we get total page in index page by divide
// 5) then we set active mode
// 6) it ensures which page we are browsing
// 7) 

$itemPerPage = $_SESSION['itemPerPage'];
$totalItem = $book->count();


$totalPage = ceil($totalItem / $itemPerPage);
//Utility::dd($itemPerPage);
$pagination = "";


if (array_key_exists('pageNumber', $_GET)) {
    $pageNumber = $_GET['pageNumber'];
} else {
    $pageNumber = 1;
}
for ($i = 1; $i <= $totalPage; $i++) {
    $class = ($pageNumber == $i) ? "active" : "";
    $pagination .= "<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom = $itemPerPage * ($pageNumber - 1);

if (strtoupper($_SERVER['REQUEST_METHOD']) == "GET")
    $allBook = $book->paginator($pageStartFrom, $itemPerPage);

if (strtoupper($_SERVER['REQUEST_METHOD']) == "POST") {
    $allBook = $book->prepareVariableValue($_POST)->index();
}
if (strtoupper($_SERVER['REQUEST_METHOD']) == "GET" && isset($_GET['search'])) {
    $allBook = $book->prepareVariableValue($_GET)->index();
}

//Utility::d($_GET);
//Utility::d($_POST);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Book Title List</h2>
    <a href="create.php" class="btn btn-info" role="button">Create Again</a>
    <a href="trashed.php" class="btn btn-info" role="button">Go to trashed page</a>
    <a href="pdf.php" class="btn btn-info" role="button">Download as pdf</a>
    <a href="xl.php" class="btn btn-info" role="button">Download as excel</a>
    <a href="mail.php" class="btn btn-info" role="button">Send mail to friend</a>

    <div id='message'>
        <?php
        if (array_key_exists('message', $_SESSION) && (!empty($_SESSION['message'])))
            echo Message::examineMessage();
        ?>
    </div>

    <p></p>
    <form role="form" action="index.php" method="get">
        <label class="control-label col-sm-2">Search</label>

        <div class="col-sm-10">
            <label> <input type="text" class="form-control" name='search' id="search"
                           placeholder="Search Book Title"></label>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
    <form role="form" action="index.php" method="post">


        <label class="control-label col-sm-2">Filter by book title:</label>

        <div class="col-sm-10">
            <label> <input type="text" class="form-control" name='filterByTitle' id="filterByTitle"
                           placeholder="Filter by book title"></label>
        </div>

        <label class="control-label col-sm-2">Filter by book description:</label>

        <div class="col-sm-10">
            <label> <input type="text" class="form-control" name='filterBydescription' id="filterBydescription"
                           placeholder="Filter by book description"></label>
        </div>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>

    <form role="form">
        <div class="form-group">
            <label for="sel1">Select homw many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option

                    <?php
                    if ($itemPerPage == 5) {

                        ?>
                        selected

                        <?php
                    }
                    ?>


                >5
                </option>
                <option
                    <?php
                    if ($itemPerPage == 10) {

                        ?>
                        selected

                        <?php
                    }
                    ?>
                >10
                </option>
                <option
                    <?php
                    if ($itemPerPage == 15) {

                        ?>
                        selected

                        <?php
                    }
                    ?>
                >15
                </option>
                <option
                    <?php
                    if ($itemPerPage == 20) {

                        ?>
                        selected

                        <?php
                    }
                    ?>
                >20
                </option>
                <option
                    <?php
                    if ($itemPerPage == 25) {

                        ?>
                        selected

                        <?php
                    }
                    ?>
                >25
                </option>
            </select>
            <button type="submit">Go!</button>

        </div>
    </form>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Title</th>
            <th>Description</th>
            <th>Email</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        <!--        Here getAllBookData is a object -->
        <?php
        $serialNumber = 1;
        foreach ($allBook as $book) {
            ?>
            <tr>
                <td><?php echo $serialNumber++ + $pageStartFrom ?></td>
                <td><?php echo $book->id ?></td>
                <td><?php echo $book->title ?></td>
                <td width="150px"><?php echo $book->description ?></td>
                <td><?php echo $book->email ?></td>
                <td>
                    <a href="view.php?id=<?php echo $book->id ?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $book->id ?>" class="btn btn-primary" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $book->id ?>" class="btn btn-danger" role="button"
                       onclick="return confirmDelete()"

                    ">Delete</a>
                    <a href="trash.php?id=<?php echo $book->id ?>" class="btn btn-warning" role="button">Trash</a>
                    <a href="singleMail.php?id=<?php echo $book->id ?>" class="btn btn-info" role="button">Mail</a>
                </td>
            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>
    <ul class="pagination">
        <li
            <?php
            if ($pageNumber == 1) {

                ?>
                class="hidden"

                <?php
            }
            ?>
        ><a href="index.php?pageNumber= <?php echo($pageNumber - 1) ?>">Prev</a></li>
        <?php echo $pagination ?>
        <li
            <?php
            if ($pageNumber == $totalPage) {

                ?>
                class="hidden"

                <?php
            }
            ?>
        ><a href="index.php?pageNumber= <?php echo($pageNumber + 1) ?>">Next</a></li>
    </ul>
</div>

<script>

    $('#message').show().delay(2000).fadeOut();

    function confirmDelete() {
        var x = confirm("Do you want to delete?");

        if (x)
            return true;
        else return false;
    }


</script>

</body>
</html>

