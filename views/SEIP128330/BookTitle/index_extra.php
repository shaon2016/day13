<?php
session_start();
include_once("../../../vendor/autoload.php");
use App\BITM\SEIP128330\BookTitle\BookTitle;
use App\BITM\SEIP128330\Utility;
use App\BITM\SEIP128330\Message;

$book = new BookTitle();

if (array_key_exists("itemPerPage", $_SESSION) &&
    array_key_exists('itemPerPage', $_GET)
)
    $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
else $_SESSION['itemPerPage'] = 5;

$itemPerPage = $_SESSION['itemPerPage'];

$totalItem = $book->count();
$totalPage = ceil($totalItem / $itemPerPage);

$pagination = "";

if (array_key_exists('pageNumber', $_GET))
    $pageNumber = $_GET['pageNumber'];
else $pageNumber = 1;

for ($i = 1; $i <= $totalPage; $i++) {
    $class = ($pageNumber == $i) ? "active" : "";
    $pagination .= "<li class='$class'><a href='index_extra.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom = $itemPerPage * ($pageNumber - 1);

$allBook = $book->paginator($pageStartFrom, $itemPerPage);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Book Title List</h2>
    <a href="create.php" class="btn btn-info" role="button">Create Again</a>
    <a href="trashed.php" class="btn btn-info" role="button">Go to trashed page</a>

    <div id='message'>
        <?php
        if (array_key_exists('message', $_SESSION) && (!empty($_SESSION['message'])))
            echo Message::examineMessage();
        ?>
    </div>


    <table class="table table-hover">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Title</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        <!--        Here getAllBookData is a object -->
        <?php
        $serialNumber = 1;
        foreach ($allBook as $book) {
            ?>
            <tr>
                <td><?php echo $serialNumber++ + $pageStartFrom ?></td>
                <td><?php echo $book->id ?></td>
                <td><?php echo $book->title ?></td>
                <td width="150px"><?php echo $book->description ?></td>
                <td>
                    <a href="view.php?id=<?php echo $book->id ?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $book->id ?>" class="btn btn-primary" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $book->id ?>" class="btn btn-danger" role="button"
                       onclick="return confirmDelete()"

                    ">Delete</a>
                    <a href="trash.php?id=<?php echo $book->id ?>" class="btn btn-warning" role="button">Trash</a>
                </td>
            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>
    <ul class="pagination">
        <li
            <?php
            if ($pageNumber == 1) {


                ?>
                class="hidden"
                <?php
            }
            ?>
        >
            <a href="index_extra.php?pageNumber=<?php echo $pageNumber - 1 ?>">prev</a>
        </li>
        <?php
        echo $pagination;
        ?>
        <li
            <?php
            if ($pageNumber == $totalPage) {


                ?>
                class="hidden"
                <?php
            }
            ?>
        >
            <a href="index_extra.php?pageNumber=<?php echo $pageNumber + 1 ?>">Next</a>
        </li>
    </ul>
    
</div>

<script>

    $('#message').show().delay(2000).fadeOut();

    function confirmDelete() {
        var x = confirm("Do you want to delete?");

        if (x)
            return true;
        else return false;
    }


</script>

</body>
</html>


