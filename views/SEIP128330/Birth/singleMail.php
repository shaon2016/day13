<?php


include_once('../../../vendor/autoload.php');

$obj = new \App\BITM\SEIP128330\Birth\Birth();
$allData = $obj->prepareVariableValue($_GET)->view();

$trs = "";


$id = $allData['id'];
$name = $allData['name'];
$date = $allData['date'];
$email = $allData['email'];


$trs .= "<tr>";
$trs .= "<td> $id</td>";
$trs .= "<td> $name </td>";
$trs .= "<td> $date </td>";
$trs .= "</tr>";

$html = <<<EDO
<div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                   
                    <th>ID</th>
                    <th>Name</th>
                    <th>Date</th>


              </tr>
                </thead>
                <tbody>

                  $trs

                </tbody>
            </table>
EDO;

include_once('../../../vendor/phpmailer/phpmailer/class.phpmailer.php');
include_once('../../../vendor/phpmailer/phpmailer/class.smtp.php');
/**
 * This example shows settings to use when sending via Google's Gmail servers.
 */
//SMTP needs accurate times, and the PHP time zone MUST be set
//This should be done in your php.ini, but this is how to do it if you don't have access to that
date_default_timezone_set('Etc/UTC');

//Create a new PHPMailer instance
$mail = new PHPMailer;
//Tell PHPMailer to use SMTP
$mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
$mail->SMTPDebug = 0;
//Ask for HTML-friendly debug output
$mail->Debugoutput = 'html';
//Set the hostname of the mail server
$mail->Host = 'smtp.gmail.com';
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
$mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
$mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
$mail->SMTPAuth = true;

$mail->SMTPOptions = array(
    'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
    )
);

//Username to use for SMTP authentication - use full email address for gmail
$mail->Username = "php.mail.practice@gmail.com";
//Password to use for SMTP authentication
$mail->Password = "php12345";
//Set who the message is to be sent from
$mail->setFrom('from@example.com', 'First Last');
//Set an alternative reply-to address
$mail->addReplyTo('replyto@example.com', 'First Last');
//Set who the message is to be sent to
$mail->addAddress($email, $name);
//Set the subject line
$mail->Subject = 'PHPMailer GMail SMTP test';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
//$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
//Replace the plain text body with one created manually
$mail->AltBody = 'This is a plain-text message body';
$mail->Body = $html;
//Attach an image file
//$mail->addAttachment('images/phpmailer_mini.png');
//send the message, check for errors
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    echo "Message sent!";
}

